import {useEffect, useState} from "react";
import {Label} from "semantic-ui-react";

export default function FieldErrors(props) {
    const [message, setMessage] = useState('')

    useEffect(() => {
        setMessage(props.errors
            .filter((error) => error.field === props.field)
            .map((error) => error.message)
            .join(','));
    }, [props.errors,props.field])

    return (
        <div>
            {message.length > 0 && <Label basic color='red' pointing>{message}</Label>}
        </div>
    )
}