export const PAGE_SIZE_OPTIONS = [
    {
        key: '10',
        text: '10',
        value: 10,
    },
    {
        key: '20',
        text: '20',
        value: 20,
    },
    {
        key: '30',
        text: '30',
        value: 30,
    }
]