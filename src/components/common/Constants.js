export const VIOLATIONS_EXCEPTION = 'violations exception';
export const PRODUCTS_PAGE_SIZE = 'products_page_size';
export const SUPPLIERS_PAGE_SIZE = 'supplier_page_size';
export const CUSTOMERS_PAGE_SIZE = 'customer_page_size';
export const EVENTS_PAGE_SIZE = 'events_page_size';