import axios from "axios";

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL
});

const PATH_EVENT = '/event';
const PATH_PRODUCT = '/product';
const PATH_CUSTOMER = '/customer';
const PATH_SUPPLIER = '/supplier';
const PATH_SALE_ITEMS = '/saleitem';

/*---------------------------- event ----------------------------*/

export const apiLoadEvents = (filter, pageNumber, pageSize) => {
    return instance.get(PATH_EVENT, {
        params: {
            name: filter,
            pageNumber: pageNumber - 1,
            pageSize: pageSize
        }
    });
}

export const apiNewEvent = (name, currency, rate) => {
    return instance.post(PATH_EVENT, {
        name,
        currency,
        rate
    })
}

export const apiUpdateEvent = (id, name, currency, rate) => {
    return instance.put(PATH_EVENT + '/' + id, {
        name,
        currency,
        rate
    })
}

export const apiDeleteEvent = (id) => {
    return instance.delete(PATH_EVENT, {
        params: {id}
    })
}

/*---------------------------- product ----------------------------*/

export const apiLoadProducts = (name, supplierId, pageNumber, pageSize) => {
    return instance.get(PATH_PRODUCT, {
        params: {
            name: name,
            supplier_id: supplierId,
            pageNumber: pageNumber - 1,
            pageSize: pageSize
        }
    });
}

export const apiProductDetails = (id) => {
    return instance.get(PATH_PRODUCT + '/card', {
        params: {
            id
        }
    });
}

export const apiUpdateProduct = (id, name, supplierId) => {
    return instance.put(PATH_PRODUCT + '/' + id, {
        name,
        supplier_id: supplierId
    })
}

// export const apiDeleteProduct = (id) => {
//     return instance.delete(PATH_PRODUCT, {
//         params: {id}
//     })
// }

export const apiNewProduct = (name, supplierId) => {
    return instance.post(`http://localhost:8080/product`, {
        name,
        supplier_id: supplierId
    })
}

/*---------------------------- customer ----------------------------*/

export const apiLoadCustomers = (filter, pageNumber, pageSize) => {
    return instance.get(PATH_CUSTOMER, {
        params: {
            name: filter,
            pageNumber: pageNumber - 1,
            pageSize: pageSize
        }
    });
}

export const apiUpdateCustomer = (id, name, count) => {
    return instance.put(PATH_CUSTOMER + '/' + id, {
        name
    })
}

export const apiDeleteCustomer = (id) => {
    return instance.delete(PATH_CUSTOMER, {
        params: {id}
    })
}

export const apiNewCustomer = (name, count) => {
    return instance.post(PATH_CUSTOMER, {
        name
    })
}

/*---------------------------- supplier ----------------------------*/

export const apiLoadSuppliers = (name, pageNumber, pageSize) => {
    return instance.get(PATH_SUPPLIER, {
        params: {
            name: name,
            pageNumber: pageNumber - 1,
            pageSize: pageSize
        }
    });
}

export const apiLoadAllSuppliers = () => {
    return instance.get(PATH_SUPPLIER + "/all");
}

export const apiUpdateSupplier = (id, name, count) => {
    return instance.put(PATH_SUPPLIER + '/' + id, {
        name
    })
}

export const apiDeleteSupplier = (id) => {
    return instance.delete(PATH_SUPPLIER, {
        params: {id}
    })
}

export const apiNewSupplier = (name, count) => {
    return instance.post(PATH_SUPPLIER, {
        name
    })
}

/*---------------------------- sale item ----------------------------*/

export const apiLoadSaleItems = (eventId) => {
    return instance.get(PATH_SALE_ITEMS + '/by_event', {
        params: {"event_id": eventId}
    })
}