import {useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import {Header, Table} from "semantic-ui-react";
import {apiLoadSaleItems} from "./common/Api";


export default function Sales() {
    const location = useLocation();
    const [id] = useState(location.state.id);
    const [saleItems, setSaleItems] = useState([])

    useEffect(() => {
        apiLoadSaleItems(id)
            .then((response) => {
                setSaleItems(response.data);
            });
    }, [id])

    const rows = saleItems.map(data => (
        <Table.Row key={data.id}>
            <Table.Cell>{data.id}</Table.Cell>
            <Table.Cell>{data.product.name}</Table.Cell>
            <Table.Cell>{data.product.count}</Table.Cell>
            <Table.Cell>{data.price}</Table.Cell>
        </Table.Row>
    ))

    return(
        <div>
            <Header as='h2'>Закупки {id}</Header>
            <Table celled structured>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Товар</Table.HeaderCell>
                        <Table.HeaderCell>Количество</Table.HeaderCell>
                        <Table.HeaderCell>Цена линейки</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
        </div>
    )
}