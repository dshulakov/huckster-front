import {Button, Form, Grid, Label} from "semantic-ui-react";
import {useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {apiDeleteEvent, apiUpdateEvent} from "../../common/Api";
import {VIOLATIONS_EXCEPTION} from "../../common/Constants";
import FieldErrors from "../../common/FieldErrors";

export default function EventEdit() {
    const navigate = useNavigate();
    const location = useLocation();
    const [name, setName] = useState(location.state.name);
    const [currency, setCurrency] = useState(location.state.currency);
    const [rate, setRate] = useState(location.state.rate);
    const [isError, setIsError] = useState(false)
    const [fieldErrors, setFieldErrors] = useState([]);

    const id = location.state.id

    const updateEvent = () => {
        apiUpdateEvent(id, name, currency, rate)
            .then(() => {
                setIsError(false);
                setFieldErrors([]);
                navigate(-1)
            }).catch((error) => {
            setIsError(true);
            if (error.response) {
                if (error.response.data.message === VIOLATIONS_EXCEPTION) {
                    setFieldErrors(error.response.data.violations);
                } else {
                    setFieldErrors([]);
                }
            }
        });
    }

    const deleteEvent = () => {
        apiDeleteEvent(id)
            .then(() => {
                navigate(-1)
            });
    }


    return (
        <div>
            <Grid>
                <Grid.Column width={4}></Grid.Column>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Field>
                            {isError && <Label size='large' color='red'>Возникла ошибка!</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Наименование</label>
                            <input type="text" placeholder='Наименование' value={name}
                                   onChange={e => setName(e.target.value)}/>
                            <FieldErrors errors={fieldErrors} field={'name'}/>
                        </Form.Field>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>Валюта</label>
                                <input type="text" placeholder='Валюта' value={currency}
                                       onChange={e => setCurrency(e.target.value)}/>
                                <FieldErrors errors={fieldErrors} field={'currency'}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Курс</label>
                                <input type="number" placeholder='Курс' value={rate}
                                       onChange={e => setRate(e.target.value)}/>
                                <FieldErrors errors={fieldErrors} field={'rate'}/>
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Button width={3} onClick={updateEvent} type={"submit"}>Сохранить</Button>
                            <Button width={3} onClick={e => navigate(-1)}>Отмена</Button>
                        </Form.Group>
                    </Form>
                    <Button onClick={deleteEvent} color='red'>Удалить</Button>
                </Grid.Column>
                <Grid.Column width={4}></Grid.Column>
            </Grid>

        </div>
    )
}