import {useEffect, useState} from "react";
import {Button, Dropdown, Grid, Header, Icon, Input, Menu, Pagination, Table} from 'semantic-ui-react'
import {Link, useNavigate, useParams} from "react-router-dom";
import {PAGE_SIZE_OPTIONS} from "../../common/PageSizeOptions";
import {apiLoadEvents} from "../../common/Api"
import {EVENTS_PAGE_SIZE} from "../../common/Constants";

export default function Event() {
    const navigate = useNavigate();
    const [APIData, setAPIData] = useState([]);
    const [name, setName] = useState('');
    const [totalPages, setTotalPages] = useState(1);
    const [pageSize, setPageSize] = useState(localStorage.getItem(EVENTS_PAGE_SIZE) === null ? 10 : localStorage.getItem(EVENTS_PAGE_SIZE));

    let {pageNumber} = useParams();

    useEffect(() => {
        apiLoadEvents(name, pageNumber, pageSize)
            .then((response) => {
                setAPIData(response.data.content);
                setTotalPages(response.data.totalPages);
            });
    }, [name, pageNumber, pageSize])

    const rows = APIData.map(data => (
        <Table.Row key={data.id}>
            <Table.Cell>{data.id}</Table.Cell>
            <Table.Cell>
                <Link to={"/sales/" + data.id}
                      state={{
                          id: data.id,
                          name: data.name,
                          currency: data.currency,
                          rate: data.rate
                      }}>
                    {data.name}
                </Link>
            </Table.Cell>
            <Table.Cell>{data.currency}</Table.Cell>
            <Table.Cell>{data.rate}</Table.Cell>
            <Table.Cell>
                <Button icon onClick={e => {
                    navigate("/event/edit", {
                        state: {
                            id: data.id,
                            name: data.name,
                            currency: data.currency,
                            rate: data.rate
                        }
                    })
                }}><Icon name='edit'/></Button>
            </Table.Cell>
        </Table.Row>
    ))

    const handlePaginationChange = (e, {activePage}) => {
        navigate("/event/page/" + activePage);
    }

    const handleRowCountChange = (e, {value}) => {
        setPageSize(value)
        localStorage.setItem(EVENTS_PAGE_SIZE, value)
    }

    return (
        <div>
            <Header as='h2'>Закупки</Header>
            <Grid celled>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Button fluid onClick={e => navigate("/event/new")}>Создать</Button>
                    </Grid.Column>
                    <Grid.Column width={7}>
                        <Input fluid value={name}
                               icon={<Icon name='delete' link onClick={() => setName('')}/>}
                               placeholder='Наименование...'
                               onChange={(e, {value}) => {
                                   setName(value)
                               }}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width={1}>ID</Table.HeaderCell>
                        <Table.HeaderCell>Наименование</Table.HeaderCell>
                        <Table.HeaderCell width={1}>Валюта</Table.HeaderCell>
                        <Table.HeaderCell width={1}>Курс</Table.HeaderCell>
                        <Table.HeaderCell width={1}></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
            <Menu>
                <Menu.Item>
                    <Pagination
                        activePage={pageNumber}
                        onPageChange={handlePaginationChange}
                        totalPages={totalPages}
                    />
                </Menu.Item>
                <Menu.Item position='right'>
                    <Dropdown
                        compact
                        selection
                        options={PAGE_SIZE_OPTIONS}
                        value={parseInt(pageSize)}
                        onChange={handleRowCountChange}
                    />
                </Menu.Item>
            </Menu>
        </div>

    )
}
