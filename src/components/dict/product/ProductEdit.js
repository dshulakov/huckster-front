import {Button, Form, Grid, Label} from "semantic-ui-react";
import {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {apiProductDetails, apiUpdateProduct} from "../../common/Api";
import {VIOLATIONS_EXCEPTION} from "../../common/Constants";
import SupplierSelection from "../supplier/SupplierSelection";
import FieldErrors from "../../common/FieldErrors";

export default function ProductEdit() {
    const navigate = useNavigate();
    const params = useParams();
    const id = params.id;
    const [name, setName] = useState('');
    const [supplierId, setSupplierId] = useState();
    const [isError, setIsError] = useState(false)
    const [fieldErrors, setFieldErrors] = useState([]);

    useEffect(() => {
        apiProductDetails(id)
            .then((response) => {
                    setName(response.data.name);
                    setSupplierId(response.data.supplier.id);
                }
            )
    }, [id])

    const updateEvent = () => {
        apiUpdateProduct(id, name, supplierId).then(() => {
            setIsError(false);
            setFieldErrors([]);
            navigate(-1)
        }).catch((error) => {
            setIsError(true);
            if (error.response) {
                if (error.response.data.message === VIOLATIONS_EXCEPTION) {
                    setFieldErrors(error.response.data.violations);
                } else {
                    setFieldErrors([]);
                }
            }
        });
    }

    // const deleteEvent = () => {
    //     apiDeleteProduct(id).then(() => {
    //         navigate(-1)
    //     });
    // }


    return (
        <div>
            <Grid>
                <Grid.Column width={4}></Grid.Column>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Field>
                            {isError && <Label size='large' color='red'>Возникла ошибка!</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Наименование</label>
                            <input type="text" placeholder='Наименование' value={name}
                                   onChange={e => setName(e.target.value)}/>
                            <FieldErrors errors={fieldErrors} field={'name'}/>
                        </Form.Field>
                        <Form.Field>
                            <label>Поставщик</label>
                            <SupplierSelection value={supplierId}
                                               selectionHandle={(e, data) => setSupplierId(data.value)}/>
                            <FieldErrors errors={fieldErrors} field={'supplierId'}/>
                        </Form.Field>
                        <Form.Group widths='equal'>
                            <Button width={3} onClick={updateEvent} type={"submit"}>Сохранить</Button>
                        </Form.Group>
                    </Form>
                </Grid.Column>
                <Grid.Column width={4}></Grid.Column>
            </Grid>

        </div>
    )
}