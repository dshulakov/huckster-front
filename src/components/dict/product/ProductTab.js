import {Button, Tab} from "semantic-ui-react";
import ProductEdit from "./ProductEdit";
import {useNavigate} from "react-router-dom";

export default function ProductTab() {
    const navigate = useNavigate();
    const panes = [
        {menuItem: 'Реквизиты', render: () => <Tab.Pane><ProductEdit/></Tab.Pane>},
        {menuItem: 'Размеры', render: () => <Tab.Pane>Tab 2 Content</Tab.Pane>}
    ]

    return (
        <div>
            <Button width={3} onClick={() => navigate(-1)} type={"submit"}>Вернуться к товарам</Button>
            <Tab menu={{fluid: true, vertical: true, tabular: true}} panes={panes}/>
        </div>
    )
}