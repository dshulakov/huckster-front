import {useEffect, useState} from "react";
import {Button, Dropdown, Grid, Header, Icon, Input, Menu, Pagination, Table} from 'semantic-ui-react'
import {Link, useNavigate, useParams, useSearchParams} from "react-router-dom";
import {PAGE_SIZE_OPTIONS} from "../../common/PageSizeOptions";
import {apiLoadProducts} from "../../common/Api";
import SupplierSelection from "../supplier/SupplierSelection";
import {PRODUCTS_PAGE_SIZE} from "../../common/Constants";

export default function Product() {
    const navigate = useNavigate();
    let [searchParams, setSearchParams] = useSearchParams();
    const [APIData, setAPIData] = useState([]);
    const [name, setName] = useState(searchParams.get('name'));
    const [supplierId, setSupplierId] = useState(searchParams.get('supplierId'));
    const [totalPages, setTotalPages] = useState(1);

    const [pageSize, setPageSize] = useState(localStorage.getItem(PRODUCTS_PAGE_SIZE) === null ? 10 : localStorage.getItem(PRODUCTS_PAGE_SIZE));

    let [pageNumber] = isNaN(searchParams.get('pageNumber')) ? '' : searchParams.get('pageNumber');

    useEffect(() => {
        apiLoadProducts(name, supplierId, searchParams.get('pageNumber'), pageSize)
            .then((response) => {
                setAPIData(response.data.content);
                setTotalPages(response.data.totalPages);
            });
    }, [searchParams, pageSize])

    const rows = APIData.map(data => (
        <Table.Row key={data.id}>
            <Table.Cell>{data.id}</Table.Cell>
            <Table.Cell>
                <Link to={"/product/edit/" + data.id}>{data.name}</Link>
            </Table.Cell>
            <Table.Cell>{data.supplier.name}</Table.Cell>
        </Table.Row>
    ))

    const handlePaginationChange = (e, {activePage}) => {
        pageNumber = activePage;

        const par = {}
        par.pageNumber = pageNumber;
        if (name) {
            par.name = name;
        }
        if (supplierId) {
            par.supplierId = supplierId;
        }
        setSearchParams(par)
        // navigate("/product/page/" + activePage);
    }

    const handleRowCountChange = (e, {value}) => {
        setPageSize(value)
        localStorage.setItem(PRODUCTS_PAGE_SIZE, value)
    }

    const handleFilterclick = () => {
        // navigate("/product/page/1?name=" + name + "&supplierId=" + supplierId);
        const par = {pageNumber: pageNumber}
        if (name) {
            par.name = name;
        }
        if (supplierId) {
            par.supplierId = supplierId;
        }
        setSearchParams(par)
    }

    return (
        <div>
            <Header as='h2'>Товары</Header>
            <Grid celled>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Button fluid onClick={() => navigate("/product/new")}>Создать</Button>
                    </Grid.Column>
                    <Grid.Column width={6}>
                        <SupplierSelection value={supplierId} selectionHandle={(e, data) => setSupplierId(data.value)}/>
                    </Grid.Column>
                    <Grid.Column width={6}>
                        <Input fluid value={name}
                               icon={<Icon name='delete' link onClick={() => setName('')}/>}
                               placeholder='Наименование...'
                               onChange={(e, {value}) => {
                                   setName(value)
                               }}/>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <Button fluid onClick={handleFilterclick}>Поиск</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Наименование</Table.HeaderCell>
                        <Table.HeaderCell>Поставщик</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
            <Menu>
                <Menu.Item>
                    <Pagination
                        activePage={pageNumber}
                        onPageChange={handlePaginationChange}
                        totalPages={totalPages}
                    />
                </Menu.Item>
                <Menu.Item position='right'>
                    <Dropdown
                        compact
                        selection
                        options={PAGE_SIZE_OPTIONS}
                        value={parseInt(pageSize)}
                        onChange={handleRowCountChange}
                    />
                </Menu.Item>
            </Menu>

        </div>

    )
}
