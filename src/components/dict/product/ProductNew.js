import {Button, Form, Grid, Label} from "semantic-ui-react";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import {apiNewProduct} from "../../common/Api";
import {VIOLATIONS_EXCEPTION} from "../../common/Constants";
import SupplierSelection from "../supplier/SupplierSelection";
import FieldErrors from "../../common/FieldErrors";

export default function ProductNew() {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [supplierId, setSupplierId] = useState();
    const [isError, setIsError] = useState(false)
    const [fieldErrors, setFieldErrors] = useState([]);

    const createProduct = () => {
        apiNewProduct(name, supplierId)
            .then(() => {
                setIsError(false);
                setFieldErrors([]);
                 navigate(-1);
            })
            .catch((error) => {
                setIsError(true);
                if (error.response) {
                    if (error.response.data.message === VIOLATIONS_EXCEPTION) {
                        setFieldErrors(error.response.data.violations);
                    } else {
                        setFieldErrors([]);
                    }
                }
            });
    }

    return (
        <div>
            <Grid>
                <Grid.Column width={4}></Grid.Column>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Field>
                            {isError && <Label size='large' color='red'>Возникла ошибка!</Label>}
                        </Form.Field>
                        <Form.Field>
                            <input type="text" placeholder='Наименование' value={name}
                                   onChange={e => setName(e.target.value)}/>
                            <FieldErrors errors={fieldErrors} field={'name'}/>
                        </Form.Field>
                        <Form.Field>
                            <label>Поставщик</label>
                            <SupplierSelection selectionHandle={(e, data) => setSupplierId(data.value)}/>
                            <FieldErrors errors={fieldErrors} field={'supplierId'}/>
                        </Form.Field>
                        <Button onClick={createProduct} type={"submit"}>Создать</Button>
                        <Button onClick={() => navigate(-1)}>Отмена</Button>
                    </Form>
                </Grid.Column>
                <Grid.Column width={4}></Grid.Column>
            </Grid>

        </div>
    )
}