import {useEffect, useState} from "react";
import {Button, Dropdown, Grid, Header, Icon, Input, Menu, Pagination, Table} from 'semantic-ui-react'
import {Link, useNavigate, useParams} from "react-router-dom";
import {PAGE_SIZE_OPTIONS} from "../../common/PageSizeOptions";
import {apiLoadSuppliers} from "../../common/Api";
import {SUPPLIERS_PAGE_SIZE} from "../../common/Constants";

export default function Supplier() {
    const navigate = useNavigate();
    const [APIData, setAPIData] = useState([]);
    const [name, setName] = useState('');
    const [totalPages, setTotalPages] = useState(1);
    const [pageSize, setPageSize] = useState(localStorage.getItem(SUPPLIERS_PAGE_SIZE) === null ? 10 : localStorage.getItem(SUPPLIERS_PAGE_SIZE));

    let {pageNumber} = useParams();

    useEffect(() => {
        apiLoadSuppliers(name, pageNumber, pageSize)
            .then((response) => {
                setAPIData(response.data.content);
                setTotalPages(response.data.totalPages);
            });
    }, [name, pageNumber, pageSize])

    const rows = APIData.map(data => (
        <Table.Row key={data.id}>
            <Table.Cell>{data.id}</Table.Cell>
            <Table.Cell>
                <Link to="/supplier/edit"
                      state={{
                          id: data.id,
                          name: data.name
                      }}>
                    {data.name}
                </Link>
            </Table.Cell>
        </Table.Row>
    ))

    const handlePaginationChange = (e, {activePage}) => {
        navigate("/supplier/page/" + activePage);
    }

    const handleRowCountChange = (e, {value}) => {
        setPageSize(value)
        localStorage.setItem(SUPPLIERS_PAGE_SIZE, value)
    }

    return (
        <div>
            <Header as='h2'>Поставщики</Header>
            <Grid celled>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Button fluid onClick={e => navigate("/supplier/new")}>Создать</Button>
                    </Grid.Column>
                    <Grid.Column width={7}>
                        <Input fluid value={name}
                               icon={<Icon name='delete' link onClick={() => setName('')}/>}
                               placeholder='Наименование...'
                               onChange={(e, {value}) => {
                                   setName(value)
                               }}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Наименование</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
            <Menu>
                <Menu.Item>
                    <Pagination
                        activePage={pageNumber}
                        onPageChange={handlePaginationChange}
                        totalPages={totalPages}
                    />
                </Menu.Item>
                <Menu.Item position='right'>
                    <Dropdown
                        compact
                        selection
                        options={PAGE_SIZE_OPTIONS}
                        value={parseInt(pageSize)}
                        onChange={handleRowCountChange}
                    />
                </Menu.Item>
            </Menu>
        </div>

    )
}
