import {Container, Dropdown} from "semantic-ui-react";
import {useEffect, useState} from "react";
import {apiLoadAllSuppliers} from "../../common/Api";

export default function SupplierSelection(props) {
    const [supplierList, setSupplierList] = useState([])

    useEffect(() => {
        apiLoadAllSuppliers()
            .then((response) => {
                setSupplierList(response.data);
            });
    }, [])

    const supplierOptions = supplierList.map(data => (
        {
            "key": data.id,
            "value": data.id,
            "text": data.name + " (" + data.id + ")"
        }
    ))

    return (
        <Container>
            <Dropdown
                placeholder='Поставщик'
                search
                selection
                fluid
                clearable
                options={supplierOptions}
                onChange={props.selectionHandle}
                value={parseInt(props.value)}
            />
        </Container>
    )
}