import {Button, Form, Grid, Label} from "semantic-ui-react";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import {apiNewSupplier} from "../../common/Api";
import {VIOLATIONS_EXCEPTION} from "../../common/Constants";
import FieldErrors from "../../common/FieldErrors";

export default function SupplierNew() {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [isError, setIsError] = useState(false)
    const [fieldErrors, setFieldErrors] = useState([]);

    const createProduct = () => {
        apiNewSupplier(name)
            .then(() => {
                setIsError(false);
                setFieldErrors([]);
                navigate(-1)
            })
            .catch((error) => {
                setIsError(true);
                if (error.response) {
                    if (error.response.data.message === VIOLATIONS_EXCEPTION) {
                        setFieldErrors(error.response.data.violations);
                    } else {
                        setFieldErrors([]);
                    }
                }
            });
    }

    return (
        <div>
            <Grid>
                <Grid.Column width={4}></Grid.Column>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Field>
                            {isError && <Label size='large' color='red'>Возникла ошибка!</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Наименование</label>
                            <input type="text" placeholder='Наименование' value={name}
                                   onChange={e => setName(e.target.value)}/>
                            <FieldErrors errors={fieldErrors} field={'name'}/>
                        </Form.Field>
                        <Button onClick={createProduct} type={"submit"}>Создать</Button>
                        <Button onClick={e => navigate(-1)}>Отмена</Button>
                    </Form>
                </Grid.Column>
                <Grid.Column width={4}></Grid.Column>
            </Grid>

        </div>
    )
}