import {useEffect, useState} from "react";
import {Button, Dropdown, Grid, Header, Icon, Input, Menu, Pagination, Table} from 'semantic-ui-react'
import {Link, useNavigate, useParams} from "react-router-dom";
import {PAGE_SIZE_OPTIONS} from "../../common/PageSizeOptions";
import {apiLoadCustomers} from "../../common/Api";
import {CUSTOMERS_PAGE_SIZE} from "../../common/Constants";

export default function Customer() {
    const navigate = useNavigate();
    const [APIData, setAPIData] = useState([]);
    const [name, setName] = useState('');
    const [totalPages, setTotalPages] = useState(1);
    const [pageSize, setPageSize] = useState(localStorage.getItem(CUSTOMERS_PAGE_SIZE) === null ? 10 : localStorage.getItem(CUSTOMERS_PAGE_SIZE));

    let {pageNumber} = useParams();

    useEffect(() => {
        apiLoadCustomers(name, pageNumber, pageSize)
            .then((response) => {
                setAPIData(response.data.content);
                setTotalPages(response.data.totalPages);
            });
    }, [name, pageNumber, pageSize])

    const rows = APIData.map(data => (
        <Table.Row key={data.id}>
            <Table.Cell>{data.id}</Table.Cell>
            <Table.Cell>
                <Link to="/customer/edit"
                      state={{
                          id: data.id,
                          name: data.name
                      }}>
                    {data.name}
                </Link>
            </Table.Cell>
        </Table.Row>
    ))

    const handlePaginationChange = (e, {activePage}) => {
        navigate("/customer/page/" + activePage);
    }

    const handleRowCountChange = (e, {value}) => {
        setPageSize(value)
        localStorage.setItem(CUSTOMERS_PAGE_SIZE, value)
    }

    return (
        <div>
            <Header as='h2'>Клиенты</Header>
            <Grid celled>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Button fluid onClick={e => navigate("/customer/new")}>Создать</Button>
                    </Grid.Column>
                    <Grid.Column width={7}>
                        <Input fluid value={name}
                               icon={<Icon name='delete' link onClick={() => setName('')}/>}
                               placeholder='Наименование...'
                               onChange={(e, {value}) => {
                                   setName(value)
                               }}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Наименование</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
            <Menu>
                <Menu.Item>
                    <Pagination
                        activePage={pageNumber}
                        onPageChange={handlePaginationChange}
                        totalPages={totalPages}
                    />
                </Menu.Item>
                <Menu.Item position='right'>
                    <Dropdown
                        compact
                        selection
                        options={PAGE_SIZE_OPTIONS}
                        value={parseInt(pageSize)}
                        onChange={handleRowCountChange}
                    />
                </Menu.Item>
            </Menu>
        </div>

    )
}
