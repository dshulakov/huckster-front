import React from 'react'
import {Menu} from 'semantic-ui-react'
import {useNavigate} from "react-router-dom";

export default function MainMenu() {
    const navigate = useNavigate();
    return (
        <Menu>
            <Menu.Item name='events' onClick={e => navigate('/event/page/1')} >
                Закупки
            </Menu.Item>
            <Menu.Item name='suppliers' onClick={e => navigate('/supplier/page/1')} >
                Поставщики
            </Menu.Item>
            <Menu.Item name='products'  onClick={e => navigate('/product?pageNumber=1')}>
                Товары
            </Menu.Item>
            <Menu.Item name='customers'  onClick={e => navigate('/customer/page/1')}>
                Клиенты
            </Menu.Item>
        </Menu>
    )
}