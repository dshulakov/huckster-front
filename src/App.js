import {BrowserRouter, Route, Routes} from "react-router-dom";
import Event from "./components/dict/event/Event";
import Product from "./components/dict/product/Product";
import MainMenu from "./components/MainMenu";
import './App.css'
import EventNew from "./components/dict/event/EventNew";
import EventEdit from "./components/dict/event/EventEdit";
import ProductNew from "./components/dict/product/ProductNew";
import Sales from "./components/Sales";
import Customer from "./components/dict/customer/Customer";
import CustomerNew from "./components/dict/customer/CustomerNew";
import CustomerEdit from "./components/dict/customer/CustomerEdit";
import Supplier from "./components/dict/supplier/Supplier";
import SupplierNew from "./components/dict/supplier/SupplierNew";
import SupplierEdit from "./components/dict/supplier/SupplierEdit";
import ProductTab from "./components/dict/product/ProductTab";

function App() {
    return (
        <div>
            <BrowserRouter >
                <MainMenu/>
                <Routes>
                    <Route path="/event/page/:pageNumber" element={<Event /> }/>
                    <Route path="/event/new" element={<EventNew /> }/>
                    <Route path="/event/edit" element={<EventEdit /> }/>

                    <Route path="/product" element={<Product/> }/>
                    <Route path="/product/new" element={<ProductNew /> }/>
                    <Route path="/product/edit/:id" element={<ProductTab /> }/>

                    <Route path="/customer/page/:pageNumber" element={<Customer/> }/>
                    <Route path="/customer/new" element={<CustomerNew /> }/>
                    <Route path="/customer/edit" element={<CustomerEdit /> }/>

                    <Route path="/supplier/page/:pageNumber" element={<Supplier/> }/>
                    <Route path="/supplier/new" element={<SupplierNew /> }/>
                    <Route path="/supplier/edit" element={<SupplierEdit /> }/>

                    <Route path="/sales/:id" element={<Sales /> }/>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
